/* REQUIRE STATEMENTS */
const Discord = require('discord.js');
//const commando = require('discord.js-commando');
const fs = require('fs');


/* CONSTANTS */
// NOTE: Please NEVER include the botId or botToken in ANY commits!
const botId = ''; // Insert your own bot's Client ID
const botToken = ''; // Insert your own bot's Token
const bot = new Discord.Client();


/* GLOBAL VARIABLES */
var customEmojisUsed = new Object();


/* FUNCTIONS */
// messagehasCustomEmoji checkes the provided string against a regex meant to
// check for the custom emoji format: <:nameOfEmoji:012345678912345678>.
// Return - True or False for if it contains anything that matches the regex
function messagehasCustomEmoji(msg) {
  var matches = msg.match('<:[^ ]+:[0-9]+>');
  if (matches != null) {
    return true;
  }

  return false;
}

// parseEmojiName breaks the string into words then extracts the names of custom
// emojis used that match the regex. The regex used will match the format,
// <:nameOfEmoji:012345678912345678>, then will parse the name from that.
// Return - Array of names of the custom emojis used
// Return - null when no custom emojis are present
// TODO: Find another way without iteration to get all regex matches
function parseEmojiName(msg) {
  var emojis = [];
  var words = msg.split(' ');

  // Iterate over all words and add any words that match the regex to a list
  for (i = 0; i < words.length; i++) {
    var emoji = words[i].match('<:[^ ]+:[0-9]+>');
    if (emoji != null) {
      emojis.push(emoji);
    }
  }

  // If the list is empty, there are no emojis so return null
  if (emojis == null) return null;

  // Transform the list of raw emoji text into a list of emoji objects
  // with relevant information such as name and the emoji itself
  for (i = 0; i < emojis.length; i++) {
    emojis[i] = {
      name: String(emojis[i]).split(':')[1],
      emoji:   emojis[i]
    }
  }

  return emojis;
}

// initialize reads in any saved statistics from the file, 'stats.json' saved in
// the local directory.
function initialize() {
  if (!fs.existsSync('stats.json')) {
    fs.closeSync(fs.openSync('stats.json', 'w'));
  }

  fs.readFile('stats.json', function(err, data) {
    if (err) {
      return console.log('failed to read stats: ' + err);
    }

    if (String(data).trim() == '') return;

    // Parse the data from stats.json into an object
    customEmojisUsed = JSON.parse(data);
  });
}

// writeStats takes the global variable where all statistics from the Discord
// bot are saved to, then writes the object to the file as a string.
function writeStats() {
  fs.writeFile('stats.json', JSON.stringify(customEmojisUsed), function(err) {
    if (err) {
      console.log(err);
      return;
    }
  });
}


/* DISCORD CLIENT EVENTS */
// Event: Ready
// After the bot has logged in successfully and has established connection to
// the Discord servers (it is 'ready'), load all possibly custome emojis into
// memory and initialize statistics.
bot.on('ready', () => {

  // Debug message for fun readability in the console
  console.log('\n|=====| TheWatcher Bot |=====|');

  bot.emojis.every(function (emoji, index) {
    // Debug statement to establish what emojis are registered
    console.log('Registering Emoji: ' + emoji.name)
    customEmojisUsed[emoji.name] = 0;
    return true;
  });

  initialize();

  // Debug statement demonstrating a successful setup
  console.log('TheWatcher is Ready!');
})

// Event: Message
// When any message is sent on the server the bot is associated with, the bot
// will read the message and parse for any custom emojis. Once it detects that
// a custom emoji was used, increment the current counter for that emoji in memory.
bot.on('message', (message) => {
  // Ignore any messages from any bots
  if (!message.author.bot) {

    // Check to see if the message has custom emojis
    if(messagehasCustomEmoji(message.content)) {

      // Parse emojis from the message
      var emojisUsed = parseEmojiName(message.content);
      if (emojisUsed != null) {

        // Increment the count in memory of any emojis from the message
        for (i = 0; i < emojisUsed.length; i++) {
          customEmojisUsed[emojisUsed[i].name] += 1;

          // If the number of times an emoji has been used is a multiple of 10,
          // send an update containing the current count to the channel it was used in.
          if (customEmojisUsed[emojisUsed[i].name] % 10 == 0) {
            message.channel.send('The ' + emojisUsed[i].emoji + ' emoji has ' +
                'been used ' + customEmojisUsed[emojisUsed[i].name] + ' times!');
          }
        }

        // WARNING: This is costly, but necessary in order to keep all
        // statistics crash-safe.
        // Save any changes in statistics to memory
        writeStats();
      }
    }
  }
});

// Log the bot into the server
bot.login(botToken);
